<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="author" content="Rafael Calearo (www.rafatheonly.com.br)" />
    <meta name="description" content="Veja aqui na Desentupipel todos os serviços que realizamos, 
    limpeza em caixas-d'água, em piscinas, em ralos em geral, caixas de gordura, vasos sanitários, 
    em pias, alem é claro da dedetisação, desentupimento em geral, hidrojateamento e fossas sépticas." />
    <meta name="keywords" content="desentupidora, desentupipel, hidrojateamento, desentupidora pelotas, 
    limpeza em caixa de agua, fossa séptica, fossas sépticas, limpeza de piscina, limpeza em piscinas,
    dedetização, dedetizacao, detetização, detetizacao"/>  
    <!--<meta name="robots" content="index">-->
    <title>Desentupipel — Desentupidora e Dedetizadora</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/favicon.ico') ?>" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url('assets/css/styles.css') ?>" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top"><span class="text-blue">DESENTUPI</span><span
                    class="text-danger">PEL</span></a>
            <button
                class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded"
                type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
                aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="#servicos">SERVIÇOS</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="#sobre">SOBRE</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="#contato">CONTATO</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a href="https://www.facebook.com/desentupidoradesentupipel" 
                            class="nav-link py-3 px-0 px-lg-3" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a href="https://www.instagram.com/desentupipel/"
                            class="nav-link py-3 px-0 px-lg-3" target="_blank"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead bg-primary text-white text-center">
        <div class="container d-flex align-items-center flex-column">
            <!-- Masthead Avatar Image-->
            <img class="masthead-avatar mb-2" src="assets/img/logo.png" alt="Logo marca da empresa" />
            <!-- Masthead Heading-->
            <h2 class="text-uppercase mb-0">DESENTUPIDORA 24 HORAS</h2>
            <!-- Masthead Subheading-->
            <p class="masthead-subheading font-weight-light mb-0"><i class="fab fa-whatsapp text-whatsapp"></i> (53)
                99130-6604 — <i class="fas fa-mobile-alt text-secondary"></i> (53) 98443-1932<br />
                <em>ORÇAMENTO GRATUITO</em>
            </p>
        </div>
    </header>
    <!-- Portfolio Section-->
    <section class="page-section portfolio" id="servicos">
        <div class="container">
            <!-- Portfolio Section Heading-->
            <h2 class="page-section-heading text-center text-uppercase text-secondary mb-2">TODOS OS SERVIÇOS</h2>
            <h5 class="text-center text-muted font-italic mb-5">Realizamos o desentupimento e limpeza ou somente a 
                limpeza para o serviço solicitado:</h5>
            <!-- Portfolio Grid Items-->
            <div class="row">
                <!-- Portfolio Item 1-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal1">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/caixa-de-gordura.png" alt="caixa de gordura" />
                    </div>
                    <h6 class="text-center text-dark">Caixa de gordura</h6>
                </div>
                <!-- Portfolio Item 2-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal2">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/vasos-sanitarios.png" alt="Vasos sanitários" />
                    </div>
                    <h6 class="text-center text-dark">Vaso sanitário</h6>
                </div>
                <!-- Portfolio Item 3-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal3">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/desentupimento-pias.png"
                            alt="Desentupimento de pias" />
                    </div>
                    <h6 class="text-center text-dark">Pia</h6>
                </div>
                <!-- Portfolio Item 4-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal4">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/ralos.png" alt="Limpeza e desentupimento em ralos" />
                    </div>
                    <h6 class="text-center text-dark">Ralo</h6>
                </div>
                <!-- Portfolio Item 5-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal5">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/caixa-dagua.png" alt="Limpeza e desentupimento em caixa-d'água" />
                    </div>
                    <h6 class="text-center text-dark">Caixa-d'água</h6>
                </div>
                <!-- Portfolio Item 6-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal6">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/piscina.png"
                            alt="Desentupimento de pias" />
                    </div>
                    <h6 class="text-center text-dark">Piscina</h6>
                </div>
                <!-- Portfolio Item 7-->
                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal7">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/hidrojateamento.png" alt="hidrojateamento" />
                    </div>
                    <h6 class="text-center text-dark">Hidrojateamento</h6>
                </div>
                <!-- Portfolio Item 8-->
                <div class="col-md-6 col-lg-4 mb-5 mb-md-0">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal8">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/fossas-septicas.png" alt="Fossas sépticas" />
                    </div>
                    <h6 class="text-center text-dark">Fossa séptica</h6>
                </div>
                <!-- Portfolio Item 9-->
                <div class="col-md-6 col-lg-4">
                    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal9">
                        <div
                            class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                            <div class="portfolio-item-caption-content text-center text-white"><i
                                    class="fas fa-plus fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/portfolio/detetizacao.png" alt="detetizacao" />
                    </div>
                    <h6 class="text-center text-dark">Dedetização</h6>
                </div>                
            </div>
        </div>
    </section>
    <!-- About Section-->
    <section class="page-section bg-primary text-white mb-0" id="sobre">
        <div class="container">
            <!-- About Section Heading-->
            <h2 class="page-section-heading text-center text-uppercase text-white">SOBRE</h2>
            <!-- Icon Divider-->
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-info"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <!-- About Section Content-->
            <div class="row">
                <div class="col-lg-4 ml-auto">
                    <p class="lead">Já precisou de uma Desentupidora em casa/trabalho? Todo o lar
                        (residência) ou qualquer tipo de comercio invariavelmente precisará de manutenções, os canos
                        (encanamento) nesses locais vai preencher inevitavelmente; depois de um tempo o encanamento fica
                        suscetível a
                        gordura e todo tipo de sujeira. Com o tempo tudo deteriora e chega infelizmente o
                        entupimento e vazamentos. Algo ruim que ninguém gosta de passar. Logo, entra o papel
                        eficaz da Desentupipel.
                    </p>
                </div>
                <div class="col-lg-4 mr-auto">
                    <p class="lead">Com mais de 5 anos de experiência, a Desentupipel Desentupidora e Dedetizadora busca
                        o máximo sempre de excelência na sua prestação de serviços. Trabalhamos 24 horas,
                        todos os dias,
                        inclusive sábados, domingos e feriados. A Desentupipel não faz só o desentupimento, a
                        empresa realiza a desobstrução, limpeza e higienização. Veja os segmentos de atuação da
                        Desentupipel: escolas, comercios, residências e etc. Peça já o seu orçamento gratuito para Pelotas-RS ou região:

                    </p>
                </div>
            </div>
            <!-- About Section Button-->
            <div class="text-center mt-4">
                <a class="btn btn-xl btn-outline-light js-scroll-trigger" href="#contato">
                    CONTATE-NOS
                </a>
            </div>
        </div>
    </section>
    <!-- Contact Section-->
    <section class="page-section" id="contato">
        <div class="container">
            <!-- Contact Section Heading-->
            <h2 class="page-section-heading text-center text-uppercase text-secondary mb-4">CONTATE-NOS</h2>
            <!-- Contact Section Form-->
            <div class="row">
                <div class="col-lg-8 mx-auto">
                <form method="post" id="contact_form">
                <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Nome completo</label>                          
                                <input class="form-control" id="nome" type="text" placeholder="Digite seu nome completo"
                                    name="nome" />
                                    <p class="help-block text-danger" id="name_error"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Endereço de e-mail</label>
                                    <input class="form-control" id="email" type="email" 
                                    placeholder="Digite seu e-mail"
                                    name="email" />
                                    <p class="help-block text-danger" id="email_error"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Telefone/celular</label>
                                    <input class="form-control" id="telefone" type="tel"
                                    placeholder="Digite seu telefone/celular (whatsapp)" name="telefone" />
                                    <p class="help-block text-danger" id="tel_error"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Digite aqui sua mensagem ou dúvida</label>
                                <textarea class="form-control" id="mensagem" rows="5"
                                    placeholder="Digite sua mensagem/dúvida" name="mensagem"></textarea>
                                    <p class="help-block text-danger" id="message_error"></p>
                            </div>
                        </div>
                        <br />
                        <div id="success">
                       
                        </div>   
                        <div class="form-group">
                            <input type="submit" name="contact" id="contact" class="btn btn-primary btn-xl" value="ENVIAR" />
                        </div>                     
				</form>
                </div>                
            </div>
            <div class="row map-responsive mt-5">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3391.7864183891866!2d-52.352726984843045!3d-31.776310781285666!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9511b5927808ac43%3A0x5eeb2aaf0f7d2530!2sR.%20Santos%20Dumont%2C%2022%20-%20Centro%2C%20Pelotas%20-%20RS%2C%2096020-380!5e0!3m2!1spt-BR!2sbr!4v1594960057992!5m2!1spt-BR!2sbr" 
                    width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </section>
    <!-- Footer-->
    <footer class="footer text-center">
        <div class="container">
            <div class="row">
                <!-- Footer Location-->
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h4 class="text-uppercase mb-4">LOCALIZAÇÃO</h4>
                    <p class="lead mb-0">
                        R. Santos Dumont, 22 (Centro)
                        <br />
                        Pelotas-RS, 96020-380
                    </p>
                </div>
                <!-- Footer Social Icons-->
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h4 class="text-uppercase mb-4">MÍDIAS SOCIAIS</h4>
                    <a class="btn btn-outline-light btn-social mx-1"
                        href="https://www.facebook.com/desentupidoradesentupipel/" target="_blank"><i
                            class="fab fa-fw fa-facebook-f"></i></a>
                    <a class="btn btn-outline-light btn-social mx-1" 
                        href="https://www.instagram.com/desentupipel/" target="_blank"><i
                            class="fab fa-fw fa-instagram"></i></a>
                </div>
                <!-- Footer About Text-->
                <div class="col-lg-4">
                    <h4 class="text-uppercase mb-4">ACEITAMOS</h4>
                    <p class="lead mb-0">
                        <img src="assets/img/cartoes/hipercard.svg" alt="Hipercard" width="50" height="30" />
                        <img src="assets/img/cartoes/visa.svg" alt="Visa" width="50" height="50" />
                        <img src="assets/img/cartoes/mastercard.svg" alt="Mastercard" width="50" height="50" />
                        <img src="assets/img/cartoes/banricompras.png" alt="Mastercard" width="30" height="30" />
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Copyright Section-->
    <div class="copyright py-4 text-center text-white">
        <div class="container"><small><?= date('Y') ?> <a href="#page-top" class="js-scroll-trigger">DESENTUPIPEL</a> • FEITO COM <i class="fas fa-heart text-danger"></i> POR RTO</small></div>
    </div>
    <a class="whatsapp-link" href="https://web.whatsapp.com/send?phone=5553991306604" target="_blank" data-toggle="tooltip" data-placement="left" title="Fale conosco agora">
		<i class="fab fa-whatsapp"></i>
	</a>
    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)
    ESSE: <div class="scroll-to-top d-lg-none position-fixed"> PARA ESSE: -->
    <div class="scroll-to-top position-fixed">
        <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i
                class="fa fa-chevron-up"></i></a>
    </div>
    <!-- Portfolio Modals-->
    <!-- Portfolio Modal 1-->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal1Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal1Label">LIMPEZA E DESENTUPIMENTO EM CAIXAS DE GORDURA</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/caixa-de-gordura.png"
                                    alt="Caixa de gordura" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">
                                    Em residências é recomendado que a limpeza da caixa de gordura seja
                                    realizada a cada seis meses, e nos prédios de três em três meses. Essa limpeza evita
                                    problemas como entupimento dos canos, mau cheiro, o escoamento lento de água na pia
                                    e a propagação de pragas urbanas como baratas e ratos. Caso o entupimento da caixa
                                    de gordura aconteça, o ideal é contratar os serviços especializados de uma
                                    desentupidora eficaz como a Desentupipel!
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 2-->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal2Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal2Label">LIMPEZA E DESENTUPIMENTO EM VASOS SANITÁRIOS</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/vasos-sanitarios.png"
                                    alt="Vasos sanitários" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">Em residências é recomendado também a limpeza da fossa séptica que
                                    recebe os detritos vindos do vaso sanitário, para que evite o problema de
                                    entupimento do vaso sanitário e canos, mau cheiro, o escoamento lento de água e a
                                    propagação de pragas urbanas. Caso ocorro o entupimento do vaso
                                    sanitário ou canos, o ideal é contratar os serviços especializados de uma
                                    desentupidora eficaz como a Desentupipel!
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 3-->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal3Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal3Label">LIMPEZA E DESENTUPIMENTO EM PIAS</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/desentupimento-pias.png"
                                    alt="Pias e ralos" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">Em residências é recomendado a limpeza do ralo da pia e seus canos, bem
                                    como de ralos em geral da casa/comercio, evitando assim o entupimento dos canos.
                                    Caso ocorro o entupimento da pia ou de algum ralo, o ideal é contratar os serviços
                                    especializados de uma desentupidora eficaz como a Desentupipel!
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 4-->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal4Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal4Label">LIMPEZA E DESENTUPIMENTO EM RALOS</h2>                                
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/ralos.png"
                                    alt="Caixa de gordura" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">
                                Em residências e em todo o tipo de segmento é recomendado a limpeza de todos 
                                os ralos existentes, evitando assim o entupimento dos canos em geral, mal 
                                cheiro e etc. Caso ocorro o entupimento da pia ou de algum ralo, o ideal é 
                                contratar os serviços especializados de uma desentupidora eficaz como a 
                                Desentupipel! 
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 5-->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal5Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal5Label">LIMPEZA E DESENTUPIMENTO EM CAIXAS-D'ÁGUA</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/caixa-dagua.png"
                                    alt="Vasos sanitários" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">Se a caixa não estiver bem higienizada, agentes patológicos podem transmitir 
                                doenças para quem consome a água. Dengue, leptospirose, diarreias e hepatite A, são apenas 
                                algumas das enfermidades causadas por uma caixa-d'água má vedada ou contaminada e sem limpeza. 
                                Orienta-se que as caixas-d’água sejam limpas regularmente de seis em seis meses. Caso seja 
                                necessário algum serviço para deixar a caixa-d'água em bom estado, o ideal é contratar os 
                                serviços especializados de uma desentupidora eficaz como a Desentupipel!
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 6-->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal6Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal6Label">LIMPEZA E DESENTUPIMENTO EM PISCINAS</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/piscina.png"
                                    alt="Limpeza e desentupimento em piscinas" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">O ideal é que você nunca deixe a sua piscina deteriorar, 
                                pois o contato constante com água de piscina mal cuidada pode trazer riscos 
                                à saúde como dermatites, conjuntivite, otite (inflamação no canal auditivo), 
                                infecções por bactérias como estafilococos, micoses e até mesmo hepatite 
                                tipo A. Outro problema comum é sujeira graúda (folhas), que causam o 
                                entupimento e paralização do motor e etc. Caso ocorro o entupimento da 
                                piscina ou de algum ralo, o ideal é contratar os serviços especializados 
                                de uma desentupidora eficaz como a Desentupipel!
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 7-->
    <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal7Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal7Label">HIDROJATEAMENTO</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/hidrojateamento.png"
                                    alt="Hidrojateamento" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">
                                    A fachada do prédio está exposta a diversos fatores que, com o passar do
                                    tempo, comprometem a sua aparência. Por isso, periodicamente é necessário fazer a
                                    sua higienização, e existem várias técnicas que podem ser empregadas para esse fim,
                                    sendo que uma delas é o hidrojateamento. Basicamente o hidrojateamento trata-se de
                                    um procedimento, onde o jato de água é aplicado com alta pressão por uma maquina
                                    (Lavadora de Alta Pressão) atingindo uma certa superfície. O objetivo final e
                                    principal deste procedimento é limpar a superfície escolhida. Além disso, o objetivo
                                    secundário também conta com a limpeza e desobstrução de locais, como tubulações.
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 8-->
    <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal8Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal8Label">FOSSA SÉPTICA</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/fossas-septicas.png"
                                    alt="Fossa séptica" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">As fossas sépticas ou séticas são unidades de tratamento primário de
                                    esgoto doméstico nas quais são feitas a separação e a transformação físico-química
                                    da matéria sólida contida no esgoto. É uma maneira simples e barata de disposição
                                    dos esgotos indicada, sobretudo, para a zona rural ou residências isoladas.
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio Modal 9-->
    <div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog"
        aria-labelledby="portfolioModal9Label" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
                <div class="modal-body text-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- Portfolio Modal - Title-->
                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-4"
                                    id="portfolioModal9Label">DEDETIZAÇÃO</h2>
                                <!-- Portfolio Modal - Image-->
                                <img class="img-fluid rounded mb-2" src="assets/img/portfolio/detetizacao.png"
                                    alt="Dedetização" />
                                <!-- Portfolio Modal - Text-->
                                <p class="mb-2">Se tem algo que tira o sossego e o conforto que temos dentro de casa é a
                                    infestação de insetos, ratos, cupins e outras pragas. Eles incomodam, comprometem
                                    todo o trabalho de limpeza da casa e, em alguns casos, ainda podem trazer doenças.
                                    Por essa razão, é fundamental fazer a dedetização de pragas de maneira periódica e
                                    planejada, a fim de preservar a higiene da sua casa e a saúde da sua família.
                                </p>
                                <button class="btn btn-primary" data-dismiss="modal">
                                    <i class="fas fa-times fa-fw"></i>
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!-- Core theme JS-->
    <script src="<?= base_url('assets/js/scripts.js') ?>"></script>
    <!-- Contact form JS-->
	<script>
    $(document).ready(function(){  
        $('#telefone').mask('(00) 00000-0000');  
        $('#contact_form').on('submit', function(event){
            event.preventDefault();                           
            $.ajax({
                url:"<?php echo base_url(); ?>home/validation",
                method:"POST",
                data:$(this).serialize(),
                dataType:"json",
                success:function(data){                                     
                    if(data.error){
                        if(data.name_error != ''){
                            $('#name_error').html(data.name_error);
                        } else {
                            $('#name_error').html('');
                        }
                        if(data.email_error != ''){
                            $('#email_error').html(data.email_error);
                        } else {
                            $('#email_error').html('');
                        }
                        if(data.tel_error != ''){
                            $('#tel_error').html(data.tel_error);
                        } else {
                            $('#tel_error').html('');
                        }
                        if(data.message_error != ''){
                            $('#message_error').html(data.message_error);
                        } else {
                            $('#message_error').html('');
                        }
                    }  
                    if(data.success){                                      
                        $('#success').html(data.success);                        
                        $('#name_error').html('');
                        $('#email_error').html('');
                        $('#tel_error').html('');
                        $('#message_error').html('');   
                        $('#nome').val('');                   
                        $('#email').val('');                   
                        $('#telefone').val('');                   
                        $('#mensagem').val('');                   
                    }                    
                }
            })
        })
    });
	$(function () {
       $('[data-toggle="tooltip"]').tooltip()
    });
	</script>
</body>

</html>